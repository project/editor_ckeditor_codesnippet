(function ($) {

/**
 * Enables code highlighting for appropriately formatted code blocks.
 */
Drupal.behaviors.editorCKEditorCodeSnippet = {
  attach: function (context, settings) {
    $('pre code', context).once('codeSnippet', function () {
      hljs.highlightBlock(this);
    });
  }
};

})(jQuery);

(function ($, Drupal) {

  'use strict';

  /**
   * Provides the summary for the "codesnippet" plugin settings vertical tab.
   */
  Drupal.behaviors.ckeditorCodeSnippetSettingsSummary = {
    attach: function attach() {
      $('#edit-editor-settings-plugins-codesnippet').drupalSetSummary(function (context) {
        var theme = $('select[name="editor[settings][plugins][codesnippet][codeSnippet_theme]"] option:selected').text();

        var vals = [];
        $('input:checked', context).next('label').each(function() {
          vals.push(Drupal.checkPlain($.trim($(this).text())));
        });

        var languages = Drupal.t('All languages');
        if (vals.length === 1) {
          languages = vals;
        }
        else if (vals.length > 1) {
          languages = vals.join(', ');
        }

        var output = '';
        output += Drupal.t('Theme: @theme, Languages: @languages, Code class: @class', {
          '@theme': theme,
          '@languages': languages,
          '@class': Drupal.checkPlain($('#edit-editor-settings-plugins-codesnippet-codesnippet-codeclass', context).val()) || Drupal.t('Requires a code class')
        });

        return output;
      });
    }
  };

})(jQuery, Drupal);

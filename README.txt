CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Technical details

INTRODUCTION
------------

Current Maintainers:

 * Devin Carlson <http://drupal.org/user/290182>

Editor CKEditor Code Snippet adds the Code Snippet plugin to Editor CKEditor.

Code Snipper provides the ability to embed code from various programming languages into CKEditor. Syntax highlighting is
enabled for both code embedded in the editor and code displayed to site visitors.

REQUIREMENTS
------------

Editor CKEditor Code Snippet has one dependency.

Contributed modules
 * Editor: CKEditor

INSTALLATION
------------

 * Install Editor CKEditor Code Snippet via the standard Drupal installation process:
   'http://drupal.org/node/895232'.
 * Enable and configure CKEditor for your desired text format at
   '/admin/config/content/formats'.
 * Move the Code Snippet button into the Active toolbar to enable it, or into the list of Available buttons to disable
   it.
 * Once enabled, a list of configuration options will become available under the Code Snippet vertical tab, below the
   CKEditor plugin settings. Select an appropriate theme, limit the languages available on the Code Snippet dialog or
   change the CSS class of the element used internally for styling.

<?php

/**
 * @file
 * Editor CKEditor hooks implemented by the Editor CKEditor Code Snippet module.
 */

/**
 * Implements hook_editor_ckeditor_plugins().
 */
function editor_ckeditor_codesnippet_editor_ckeditor_plugins() {
  $path = drupal_get_path('module', 'editor_ckeditor_codesnippet');

  // Add the codesnippet plugin.
  $plugins['codesnippet'] = array(
    'label' => t('Code Snippet'),
    'file' => $path . '/plugins/editor_ckeditor_codesnippet.codesnippet.inc',
    'plugin' => $path . '/lib/codesnippet/plugin.js',
    'internal' => FALSE,
    'buttons' => array(
      'CodeSnippet' => array(
        'label' => t('Code snippet'),
        'required_html' => array(
          array(
            'tags' => array('pre', 'code'),
            'attributes' => array('language'),
          ),
        ),
        'image' => drupal_get_path('module', 'editor_ckeditor_codesnippet') . '/lib/codesnippet/icons/codesnippet.png',
      ),
    ),
    'supports_configuration' => TRUE,
    'config callback' => 'editor_ckeditor_codesnippet_config',
    'settings form' => 'editor_ckeditor_ckeditor_plugin_codesnippet_settings_form',
  );

  return $plugins;
}

/**
 * Implements hook_editor_ckeditor_css_alter().
 */
function editor_ckeditor_codesnippet_editor_ckeditor_css_alter(array &$css, $format) {
  // Add the appropriate codesnippet theme CSS to the editor if the codesnippet
  // plugin is enabled for the current format.
  if (isset($format->editor_settings['plugins']['codesnippet'])) {
    $css[] = drupal_get_path('module', 'editor_ckeditor_codesnippet') . '/lib/codesnippet/lib/highlight/styles/' . $format->editor_settings['plugins']['codesnippet']['codeSnippet_theme'] . '.css';
  }
}

<?php

/**
 * @file
 * Defines the "codesnippet" plugin.
 */

/**
 * Config for the "codesnippet" CKEditor plugin.
 */
function editor_ckeditor_codesnippet_config($format) {
  $config = array();
  $settings = $format->editor_settings;

  if (!isset($settings['plugins']['codesnippet'])) {
    return $config;
  }

  $config = $settings['plugins']['codesnippet'];

  // Reasonable defaults that provide expected basic behavior.
  $config += array(
    'codeSnippet_theme' => 'default',
    'codeSnippet_languages' => array(),
    'codeSnippet_codeClass' => 'hljs',
  );

  // All enabled/disabled languages are stored in a single associative array.
  // Disabled languages have a value of 0 and should be filtered out so that
  // they aren't passed on to the codesnippet plugin.
  $languages = $settings['plugins']['codesnippet']['codeSnippet_languages'];
  $config['codeSnippet_languages'] = array_filter($languages);

  return $config;
}

/**
 * Settings form for the "codesnippet" CKEditor plugin.
 */
function editor_ckeditor_ckeditor_plugin_codesnippet_settings_form ($form, &$form_state, $format) {
  $settings = isset($format->editor_settings['plugins']['codesnippet']) ? $format->editor_settings['plugins']['codesnippet'] : array();

  // Defaults.
  $settings += array(
    'codeSnippet_theme' => 'default',
    'codeSnippet_languages' => array(),
    'codeSnippet_codeClass' => 'hljs',
  );

  $themes = array(
    'monokai_sublime' => t('Monokai Sublime'),
    'default' => t('Default'),
    'arta' => t('Arta'),
    'ascetic' => t('Ascetic'),
    'atelier-dune.dark' => t('Base16 Atelier Dune Dark'),
    'atelier-dune.light' => t('Base16 Atelier Dune Light'),
    'atelier-forest.dark' => t('Base16 Atelier Forest Dark'),
    'atelier-forest.light' => t('Base16 Atelier Forest Light'),
    'atelier-heath.dark' => t('Base16 Atelier Heath Dark'),
    'atelier-heath.light' => t('Base16 Atelier Heath Light'),
    'atelier-lakeside.dark' => t('Base16 Atelier Lakeside Dark'),
    'atelier-lakeside.light' => t('Base16 Atelier Lakeside Light'),
    'atelier-seaside.dark' => t('Base16 Atelier Seaside Dark'),
    'atelier-seaside.light' => t('Base16 Atelier Seaside Light'),
    'brown_paper' => t('Brown Paper'),
    'dark' => t('Dark'),
    'docco' => t('Docco'),
    'far' => t('FAR'),
    'foundation' => t('Foundation'),
    'github' => t('GitHub'),
    'googlecode' => t('Google Code'),
    'idea' => t('Intellij Idea'),
    'ir_black' => t('IR_Black'),
    'magula' => t('Magula'),
    'mono-blue' => t('Mono Blue'),
    'monokai' => t('Monokai'),
    'obsidian' => t('Obsidian'),
    'paraiso.dark' => t('Paraíso (dark)'),
    'paraiso.light' => t('Paraíso (light)'),
    'pojoaque' => t('Pojoaque'),
    'railscasts' => t('Railscasts'),
    'rainbow' => t('Rainbow'),
    'school_book' => t('School Book'),
    'solarized_dark' => t('Solarized (dark)'),
    'solarized_light' => t('Solarized (light)'),
    'sunburst' => t('Sunburst'),
    'tomorrow-night-blue' => t('Tomorrow Night Blue'),
    'tomorrow-night-bright' => t('Tomorrow Night Bright'),
    'tomorrow-night-eighties' => t('Tomorrow Night Eighties'),
    'tomorrow-night' => t('Tomorrow Night'),
    'tomorrow' => t('Tomorrow'),
    'vs' => t('Visual Studio'),
    'xcode' => t('XCode'),
    'zenburn' => t('Zenburn'),
  );

  $form['codeSnippet_theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#options' => $themes,
    '#default_value' => $settings['codeSnippet_theme'],
    '#description' => t('The theme used to render code snippets.'),
    '#attached' => array(
      'library' => array(array('editor_ckeditor_codesnippet', 'editor_ckeditor_codesnippet.admin')),
    ),
  );

  $languages = array(
    'apache' => 'Apache',
    'bash' => 'Bash',
    'coffeescript' => 'CoffeeScript',
    'cpp' => 'C++',
    'cs' => 'C#',
    'css' => 'CSS',
    'diff' => 'Diff',
    'html' => 'HTML',
    'http' => 'HTTP',
    'ini' => 'INI',
    'java' => 'Java',
    'javascript' => 'JavaScript',
    'json' => 'JSON',
    'makefile' => 'Makefile',
    'markdown' => 'Markdown',
    'nginx' => 'Nginx',
    'objectivec' => 'Objective-C',
    'perl' => 'Perl',
    'php' => 'PHP',
    'python' => 'Python',
    'ruby' => 'Ruby',
    'sql' => 'SQL',
    'vbscript' => 'VBScript',
    'xhtml' => 'XHTML',
    'xml' => 'XML',
  );

  $form['codeSnippet_languages'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Languages'),
    '#options' => $languages,
    '#default_value' => $settings['codeSnippet_languages'],
    '#description' => t('Restricts languages available in the %dialog dialog window. An empty value is always added to the list.', array('%dialog' => 'Code Snippet')),
  );

  $form['codeSnippet_codeClass'] = array(
    '#type' => 'textfield',
    '#default_value' => $settings['codeSnippet_codeClass'],
    '#title' => t('Code class'),
    '#description' => t('The CSS class of the %code element used internally for styling. Defaults to %hljs.', array('%code' => '<code>', '%hljs' => 'hljs')),
  );

  return $form;
}
